package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KimbroughServer {

    public static final int PORT_NUMBER = 11122;

    public static void main(String args[]) throws Exception {

        // Start the server for retrieving datagram packets.
        DatagramSocket socket = new DatagramSocket(PORT_NUMBER);
        System.out.println("Server ready");

        // Continuously listen for packets on the datagram socket.
        while (true) {
            byte[] receiveBuffer = new byte[1024];
            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);

            // Receive a packet from a client.
            socket.receive(receivePacket);
            System.out.println("Received a packet");

            // Hand the packet over to the handler.
            new Handler(receivePacket, socket);
        }
    }
}

class Handler implements Runnable {

    DatagramSocket socket;
    DatagramPacket packet;

    Handler(DatagramPacket packet, DatagramSocket socket) {
        this.packet = packet;
        this.socket = socket;
        new Thread(this).start();
    }

    /**
     * This location in the code should handle all the logic for obtaining new
     * packets. Check here for if the message contains a URL, "OK", or "FAIL".
     */
    public void run() {

        try {
            String webAddressContents;
            
            // Extract data and client information from the incoming packet.
            String inPacketContents = new String(packet.getData(), 0, packet.getLength());
            InetAddress address = packet.getAddress();
            int port = packet.getPort();

            if (inPacketContents.equals("OK")) {
                System.out.println("DONE");
            } 
            else if (inPacketContents.equals("FAIL")) {
                System.out.println("RETRANSMITTING");
                webAddressContents = getDataFromWebAddress(inPacketContents);
                sendPacketsToClient(webAddressContents, address, port);
            } 
            else {           
                webAddressContents = getDataFromWebAddress(inPacketContents);
                sendPacketsToClient(webAddressContents, address, port);
            }

        } 
        catch (IOException exception) {
            System.out.println(exception);
        } catch (Exception ex) {
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getDataFromWebAddress(String webAddress) throws Exception {

        // Establish an HTTP connection.
        URL url = new URL(webAddress);
        URLConnection urlConnection = url.openConnection();
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();

        // Get contents from the website.
        String line;
        InputStream in = httpConnection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder result = new StringBuilder();
        
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }
        
        return result.toString();
    }
    
    public void sendPacketsToClient(String contentsToSend, InetAddress address, int port) throws Exception {
        // Send the first response with the following info:
        // Sequence number: 0
        // Payload length: 0
        // Payload: Integer value with total number of bytes retrieved from the HTTP request
        Message message = new Message(0, 0, Integer.toString(contentsToSend.length()));
        DatagramPacket responsePacket = new DatagramPacket(message.toString().getBytes(), message.toString().getBytes().length, address, port);

        // send result to the client
        socket.send(responsePacket);

        // Break the string result into chunks.
        int chunkSize = 1024;
        String[] chunks = contentsToSend.split("(?<=\\G.{" + chunkSize + "})");

        // Send the chunks of data but only their sequence numbers
        for (int i = 0; i < chunks.length; i++) {
            message = new Message(i + 1, chunks[i].length(), chunks[i]);
            responsePacket = new DatagramPacket(message.toString().getBytes(),
                    message.toString().getBytes().length,
                    address, 
                    port);
            socket.send(responsePacket);
        }
    }
}

class Message {

    int sequenceNumber;
    int payloadLength;
    String payload;

    public Message(int sequenceNumber, int payloadLength, String payload) {
        this.sequenceNumber = sequenceNumber;
        this.payloadLength = payloadLength;
        this.payload = payload;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public int getPayloadLength() {
        return payloadLength;
    }

    public String getPayload() {
        return payload;
    }

    public String toString() {
        return "Sequence Number: " + sequenceNumber + "\n"
                + "Payload length: " + payloadLength + "\n"
                + "Payload: " + payload + "\n";
    }
}
