package socket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Scanner;
  
/**
 */
public class KimbroughClient {
    
    static final int PORT_NUMBER = 11122;
    static final int ATTEMPT_LIMIT = 2;
    
    public static void main(String args[]) throws IOException {
        
        // Create the socket object for carrying data.
        DatagramSocket socket = new DatagramSocket();
        InetAddress ip = InetAddress.getLocalHost();
        
        // Objects for sending and receiving packets.
        byte[] inBuffer = new byte[10000];
        DatagramPacket inPacket;
        DatagramPacket outPacket;    
        
        // Variables for message-exchange with server.
        int totalPayloadSize;
        int numberOfBytesReceived;
        
        // Scanner for obtaining user input.
        Scanner input = new Scanner(System.in);
        
        // Input-capture variables.
        int timeOutInSeconds;
        String targetWebAddress;
        
        // TEST VALUES
        //timeOutInSeconds = 1;
        //targetWebAddress = "https://www.towson.edu";
        
        // PROMPT: timeout in seconds.
        System.out.print("Enter a timeout period in seconds: ");
        timeOutInSeconds = input.nextInt();
        
        // PROMPT: web address.
        System.out.print("Enter the target web address (ex. 'https://www.towson.edu'^): ");
        targetWebAddress = input.next();
        System.out.println();
        input.nextLine();
        
        // Display the information entered.
        System.out.println("Timeout: " + timeOutInSeconds);
        System.out.println("Web address: " + targetWebAddress + "\n");
        
        outPacket = new DatagramPacket(targetWebAddress.getBytes(), 
                                     targetWebAddress.getBytes().length, 
                                    ip, 
                                      PORT_NUMBER);
        
        inPacket = new DatagramPacket(inBuffer,
                                      inBuffer.length,
                                      ip,
                                      PORT_NUMBER);

        int attemptsTried = 0;
        
        while (attemptsTried < ATTEMPT_LIMIT) {
            // Send the obtained web address to the server.
            sendWebAddressToServer(socket, outPacket, ip, targetWebAddress);

            // Get the total number of bytes which the server will send back to the client.
            totalPayloadSize = getPayloadLengthFromSequenceZeroPacket(socket, inPacket, ip);
            System.out.println("Total number of bytes to receive: " + totalPayloadSize);

            // Get all the messages sent from the server.
            ArrayList<Message> receivedMessages = receiveMessagesFromServer(timeOutInSeconds,
                    totalPayloadSize,
                    socket,
                    inPacket);

            // Calculate the number of bytes received
            numberOfBytesReceived = getNumberOfBytesReceived(receivedMessages);

            if (numberOfBytesReceived < totalPayloadSize) {
                System.out.println("Failed to get all content before the timeout.");

                outPacket = new DatagramPacket("FAIL".getBytes(),
                        "FAIL".getBytes().length,
                        ip,
                        PORT_NUMBER);
                socket.send(outPacket);
                
                attemptsTried++;
            } 
            else {
                outPacket = new DatagramPacket("OK".getBytes(),
                        "OK".getBytes().length,
                        ip,
                        PORT_NUMBER);

                socket.send(outPacket);

                // Display the messages received from the server.
                for (Message message : receivedMessages) {
                    if (message != null) {
                        System.out.println(message);
                    }
                }
                
                break;
            }
        }      
    }
    
    public static void sendWebAddressToServer(DatagramSocket socket, 
                                              DatagramPacket outPacket,
                                              InetAddress ip, 
                                              String targetWebAddress) throws IOException {
        

        socket.send(outPacket);
    }
    
    public static int getPayloadLengthFromSequenceZeroPacket(DatagramSocket socket,
                                                             DatagramPacket inPacket,
                                                             InetAddress ip) throws IOException {
        String data;
        
        socket.receive(inPacket);
        data = new String(inPacket.getData(), 0, inPacket.getLength());
        int totalPayloadSize = Integer.parseInt(data.split("\n")[2].split(": ")[1]);
        
        return totalPayloadSize;
    }
    
    public static ArrayList<Message> receiveMessagesFromServer(int timeOutInSeconds,
                                                               int totalPayloadSize,
                                                               DatagramSocket socket,
                                                               DatagramPacket inPacket) throws IOException {
        
        // Fill the ArrayList with dummy values. This is so that we can insert
        // elements in any location within bounds.
        ArrayList<Message> receivedMessages = new ArrayList<>(10000);
        for (int i = 0; i < 10000; i++)
            receivedMessages.add(null);
        
        String data;
        String payload;
        Message currentMessage;
        
        int sequenceNumber;
        int payloadLength;
        int payloadBytesReceived = 0;
        
        socket.setSoTimeout(timeOutInSeconds * 1000);
                
        while (payloadBytesReceived < totalPayloadSize) {
            try {
                // Retrieve the packet from the socket.
                socket.receive(inPacket);
                data = new String(inPacket.getData(), 0, inPacket.getLength());

                // Extract the sequence number, payload length, and payload from the packet.
                String[] lines = data.split("\n");
                sequenceNumber = Integer.parseInt(lines[0].split(": ")[1]);
                payloadLength = Integer.parseInt(lines[1].split(": ")[1]);
                payload = lines[2].split(": ")[1];

                // Add the message to the list of extracted packets.
                currentMessage = new Message(sequenceNumber, payloadLength, payload);
                receivedMessages.add(sequenceNumber, currentMessage);

                // Update the number of bytes received.
                payloadBytesReceived += payloadLength;
            } 
            catch (SocketTimeoutException exception) {
                System.out.println("CLIENT TIMED OUT!");
                return receivedMessages;
            }
        }
        
        return receivedMessages;
    }
    
    public static int getNumberOfBytesReceived(ArrayList<Message> receivedMessages) {
        int payloadBytesReceived = 0;
        
        for (Message message : receivedMessages) {
            if (message != null) {
                payloadBytesReceived += message.getPayloadLength();
            }
        }
        
        return payloadBytesReceived;
    }
}